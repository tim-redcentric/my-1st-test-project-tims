resource "aws_route_table" "route-table" {
  vpc_id = aws_vpc.sandbox-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW-01.id
  }
  tags = {
    "Name" = "sandbox-route-table-timsamanchi"
  }
}
resource "aws_route_table_association" "route-pub" {
  route_table_id = aws_route_table.route-table.id
  subnet_id      = aws_subnet.pub.id
}