resource "aws_vpc" "sandbox-vpc" {
  cidr_block       = var.vpc-cidr
  instance_tenancy = "default"

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    "Name" = "sandbox-test-vpc-timsamanchi"
  }
}