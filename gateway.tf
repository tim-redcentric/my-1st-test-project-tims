resource "aws_internet_gateway" "IGW-01" {
  vpc_id = aws_vpc.sandbox-vpc.id

  tags = {
    "Name" = "sandbox-test-IGW-timsamanchi"
  }
}