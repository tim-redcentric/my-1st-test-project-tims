resource "aws_subnet" "pub" {
  vpc_id     = aws_vpc.sandbox-vpc.id
  cidr_block = var.pub-cidr

  map_public_ip_on_launch = true

  availability_zone = data.aws_availability_zones.pub-az.names[1]

  tags = {
    "Name" = "sandbox-public-subnet-timsamanchi"
  }
}