variable "ssh-key-name" {}
variable "ssh-key-path" {}
variable "aws-region" {
  default = "eu-west-1"
}
variable "instance-count" {
  type = number
  default = 1
}
variable "vpc-cidr" {
  default = "10.10.0.0/16"
}
variable "pub-cidr" {
  default = "10.10.0.0/17"
}
variable "Ubuntu-ami" {
  default     = "ami-01dd271720c1ba44f"
  description = "Canonical, Ubuntu, 22.04 LTS, amd64 jammy image build on 2023-05-16"
}
variable "instance-type" {
  default = "t2.micro"
}
variable "environment-list" {
  type = list(string)
  default = [ "DEV","QA","STAGE","PROD" ]
}
variable "function-map" {
  type = map(string)

  default = {
    "su" = "SUPPORT",
    "tr" = "TRAINING",
    "rd" = "R&D",
    "sv" = "SERVICES"
  }
}
